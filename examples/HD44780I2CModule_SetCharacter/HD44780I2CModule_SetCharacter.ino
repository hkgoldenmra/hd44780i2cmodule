#include <HD44780I2CModule.h>

HD44780I2CModule hd44780I2CModule = HD44780I2CModule(A0, A1);

void setup() {
	hd44780I2CModule.start(0x27, I2CModule::WRITE, HD44780Module::ROWS_2, HD44780Module::DOTS_8);
	hd44780I2CModule.setDisplay(true, false, false);
	hd44780I2CModule.setEntry(HD44780Module::RIGHT, HD44780Module::CURSOR);
	for (byte i = 0; i < 8; i++) {
		byte data[] = {0x0E, 0x1B, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11};
		for (byte j = 8 - i; j < 8; j++) {
			data[j] |= 0x0E;
		}
		hd44780I2CModule.setCharacter(i, data);
	}
	hd44780I2CModule.clear();
	hd44780I2CModule.setText("Battery:");
	for (byte i = 0; i < 8; i++) {
		hd44780I2CModule.setCode(i);
	}
	hd44780I2CModule.setPosition(1, 2);
	hd44780I2CModule.setText("Charging...");
	hd44780I2CModule.setCode(0);
}

void loop() {
	for (byte i = 0; i < 8; i++) {
		delay(250);
		hd44780I2CModule.setPosition(1, 13);
		hd44780I2CModule.setCode(i);
	}
}