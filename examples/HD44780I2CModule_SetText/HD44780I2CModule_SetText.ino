#include <HD44780I2CModule.h>

HD44780I2CModule hd44780I2CModule = HD44780I2CModule(A0, A1);
String text = "hello, world";

void setup() {
	hd44780I2CModule.start(0x27, HD44780I2CModule::WRITE, HD44780Module::ROWS_2, HD44780Module::DOTS_8);
	hd44780I2CModule.setDisplay(true, true, true);
	hd44780I2CModule.setEntry(HD44780Module::RIGHT, HD44780Module::CURSOR);
	hd44780I2CModule.clear();
	hd44780I2CModule.setText(text);
}

void loop() {
	delay(1000);
	hd44780I2CModule.home();
	const unsigned int LENGTH = text.length();
	for (unsigned int i = 0; i < LENGTH; i++) {
		delay(1000);
		hd44780I2CModule.cursorMoveNext();
	}
}