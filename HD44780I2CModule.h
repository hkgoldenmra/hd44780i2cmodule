#ifndef HD44780I2CMODULE_H
#define HD44780I2CMODULE_H

#include <I2CModule.h>
#include <HD44780Module.h>

class HD44780I2CModule {
	private:
		I2CModule i2cModule = I2CModule();
		static const byte LENGTH = 8;
		void writeRegister(bool, byte);
		void setMovement(bool, bool);
		void setFunction(bool, bool);
	public:
		HD44780I2CModule(byte, byte);
		HD44780I2CModule();
		void start(byte, bool, bool, bool);
		void start(byte, bool);
		void clear();
		void home();
		void setEntry(bool, bool);
		void cursorMovePrevious();
		void cursorMoveNext();
		void screenMovePrevious();
		void screenMoveNext();
		void setDisplay(bool, bool, bool);
		void setCharacter(byte, byte[HD44780I2CModule::LENGTH]);
		void setPosition(byte, byte);
		void setPosition(byte);
		void setCode(byte);
		void setText(String);
};

#endif