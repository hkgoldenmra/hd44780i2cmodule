#include <HD44780I2CModule.h>

void HD44780I2CModule::writeRegister(bool rs, byte data) {
	byte head = (data & 0xF0) | 0x08;
	byte tail = ((data & 0x0F) << 4) | 0x08;
	if (rs) {
		head |= 0x01;
		tail |= 0x01;
	}
	this->i2cModule.transfer(head | 0x04, I2CModule::WRITE);
	this->i2cModule.transfer(head, I2CModule::WRITE);
	this->i2cModule.transfer(tail | 0x04, I2CModule::WRITE);
	this->i2cModule.transfer(tail, I2CModule::WRITE);
}

void HD44780I2CModule::setMovement(bool type, bool direction) {
	byte data = 0x10;
	if (type) {
		data |= 0x08;
	}
	if (direction) {
		data |= 0x04;
	}
	this->writeRegister(false, data);
}

void HD44780I2CModule::setFunction(bool rows, bool dots) {
	byte data = 0x20;
	if (rows) {
		data |= 0x08;
	}
	if (dots) {
		data |= 0x04;
	}
	this->writeRegister(false, data);
}

HD44780I2CModule::HD44780I2CModule(byte sdaPin, byte sclPin) {
	this->i2cModule = I2CModule(sdaPin, sclPin);
}

HD44780I2CModule::HD44780I2CModule() :
HD44780I2CModule::HD44780I2CModule(SDA, SCL) {
}

void HD44780I2CModule::start(byte address, bool rw, bool rows, bool dots) {
	this->i2cModule.start(address, rw);
	this->setFunction(false, false);
	this->setFunction(rows, dots);
}

void HD44780I2CModule::clear() {
	this->writeRegister(false, 0x01);
}

void HD44780I2CModule::home() {
	this->writeRegister(false, 0x02);
}

void HD44780I2CModule::setEntry(bool direction, bool type) {
	byte data = 0x04;
	if (direction) {
		data |= 0x02;
	}
	if (type) {
		data |= 0x01;
	}
	this->writeRegister(LOW, data);
}

void HD44780I2CModule::cursorMovePrevious() {
	this->setMovement(HD44780Module::CURSOR, HD44780Module::PREVIOUS);
}

void HD44780I2CModule::cursorMoveNext() {
	this->setMovement(HD44780Module::CURSOR, HD44780Module::NEXT);
}

void HD44780I2CModule::screenMovePrevious() {
	this->setMovement(HD44780Module::SCREEN, HD44780Module::PREVIOUS);
}

void HD44780I2CModule::screenMoveNext() {
	this->setMovement(HD44780Module::SCREEN, HD44780Module::NEXT);
}

void HD44780I2CModule::setDisplay(bool screen, bool cursor, bool blink) {
	byte data = 0x08;
	if (screen) {
		data |= 0x04;
	}
	if (cursor) {
		data |= 0x02;
	}
	if (blink) {
		data |= 0x01;
	}
	this->writeRegister(false, data);
}

void HD44780I2CModule::setCharacter(byte address, byte data[HD44780I2CModule::LENGTH]) {
	this->writeRegister(false, 0x40 | ((address & 0x07) << 3));
	for (byte i = 0; i < HD44780I2CModule::LENGTH; i++) {
		this->setCode(data[i]);
	}
}

void HD44780I2CModule::setPosition(byte row, byte column) {
	byte data = 0x80;
	if (row > 0) {
		data |= 0x40;
	}
	if (column > 39) {
		column = 39;
	}
	this->writeRegister(false, data + column);
}

void HD44780I2CModule::setPosition(byte position) {
	this->setPosition((position & 0x40) > 0, position & 0x3F);
}

void HD44780I2CModule::setCode(byte data) {
	this->writeRegister(true, data);
}

void HD44780I2CModule::setText(String text) {
	const unsigned int LENGTH = text.length();
	for (unsigned int i = 0; i < LENGTH; i++) {
		this->setCode(text.charAt(i));
	}
}